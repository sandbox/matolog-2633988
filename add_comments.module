<?php

/**
 * @file
 * Addcomments module that allows add comments from different users from admin panel.
 */

/**
 * Implements hook_menu().
 */
function add_comments_menu() {
  $items['admin/content/comment/addcomments'] = array(
    'title' => 'Add comment',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('add_comments_form'),
    'access arguments' => array('administer comments'),
    'type' => MENU_LOCAL_TASK,
  );
  return $items;
}

/**
 * Define an Addcomments form.
 */
function add_comments_form() {
  $form['markup'] = array(
    '#prefix' => '<div id="addcommentsinfo">',
    '#suffix' => '</div>',
    '#markup' => '',
  );
  $form['addcomment']['username'] = array(
    '#type' => 'entityreference',
    '#title' => t('Users name'),
    '#description' => t('If not set, will be use Guest as a comments name'),
    '#era_entity_type' => 'user',
    '#era_bundles' => array(),
    '#era_cardinality' => 3,
  );
  $form['addcomment']['node'] = array(
    '#type' => 'entityreference',
    '#title' => t('Attach comments to node'),
    '#required' => TRUE,
    '#era_entity_type' => 'node',
    '#era_bundles' => array(),
    '#era_cardinality' => 3,
  );
  $form['addcomment']['subject'] = array(
    '#type' => 'textfield',
    '#title' => t('Subject'),
    '#size' => 60,
    '#maxlength' => 128,
  );

  $form['addcomment']['date'] = array(
    '#type' => 'date_popup',
    '#title' => t("Comments date"),
    '#description' => t('Set comments date'),
    '#date_format' => 'Y-m-d h:i:s',
    '#attributes' => array('autocomplete' => 'off', 'readonly' => 'readonly'),
    '#default_value' => date('Y-m-d h:i:s'),
  );
  $form['addcomment']['text'] = array(
    '#type' => 'text_format',
    '#title' => t('Comments text'),
    '#format' => NULL,
  );
  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Add comment'),
    "#ajax" => array(
      "callback" => "add_comments_form_callback",
      "method" => "replace",
      "wrapper" => "addcommentsinfo",
      "effect" => "fade"
    )
  );

  return $form;
}

/**
 * Define an Addcomments form callback.
 */
function add_comments_form_callback($form, &$fstate) {
  $fstate['rebuild'] = TRUE;
  $addcomments_name = $fstate['values']['username'];
  $addcomments_node = $fstate['values']['node'];
  $addcomments_subject = $fstate['values']['subject'];
  $addcomments_date = $fstate['values']['date'];
  $addcomments_text = $fstate['values']['text'];

  if ($addcomments_name) {
    foreach ($addcomments_name as $n => $k) {
      $addcomments_user = user_load($k['entity_id']);
    }
  }
  if ($addcomments_node) {
    foreach ($addcomments_node as $n => $k) {
      $addcomments_nodeobject = node_load($n);
    }
  }
  //Create new comment
  $comment = new stdClass();
  $comment->nid = $addcomments_nodeobject->nid; // nid of a node you want to attach a comment to
  $comment->cid = 0;
  $comment->pid = 0;
  if ($addcomments_name) {
    $comment->uid = 1;
  }
  else {
    $comment->uid = 0;
  }
  if ($addcomments_name) {
    $comment->mail = $addcomments_user->mail;
    $comment->name = $addcomments_user->name;
  }
  $comment->hostname = '127.0.0.1'; // OPTIONAL. You can log poster's ip here
  if ($addcomments_date) {
    $comment->date = strtotime($addcomments_date);
  }
  if ($addcomments_name) {
    $comment->is_anonymous = 0;
  }
  else {
    $comment->is_anonymous = 1;
  }
  $comment->homepage = ''; // you can add homepage URL here
  $comment->status = COMMENT_PUBLISHED; // We auto-publish this comment
  $comment->language = $addcomments_nodeobject->language; // The same as for a node
  $comment->subject = $addcomments_subject;
  $comment->comment_body[$comment->language][0]['value'] = $addcomments_text['value'];
  $comment->comment_body[$comment->language][0]['format'] = $addcomments_text['format'];
  $addcomments_result = comment_submit($comment); // saving a comment
  comment_save($comment);
  if ($addcomments_result) {
    $form['markup'][] = drupal_set_message(t('Comment added'), 'status');
  }
  else {
    $form['markup'][] = drupal_set_message(t('Comment not added'), 'error');
  }
  return render($form['markup']);
}